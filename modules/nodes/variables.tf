variable "project_id" {
}

variable "region" {
}

variable "zone" {
}

variable "gke_initial_node_count" {
}

variable "gke_version" {
}

variable "gke_max_pods_per_node" {
}

variable "gke_node_selector" {
}

variable "environment" {
}