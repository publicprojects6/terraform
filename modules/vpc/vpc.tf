# VPC
resource "google_compute_network" "vpc" {
  project                 = var.project_id
  name                    = "${var.project_id}-${var.environment}-vpc"
  auto_create_subnetworks = "false"
}

# Subnet
resource "google_compute_subnetwork" "subnet" {
  project       = var.project_id
  depends_on    = [google_compute_network.vpc]
  name          = "${var.project_id}-${var.environment}-subnet"
  region        = var.region
  network       = "${var.project_id}-${var.environment}-vpc"
  ip_cidr_range = "10.10.0.0/24"
}
