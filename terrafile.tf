module "vpc" {
  source      = "./modules/vpc"
  project_id  = var.project_id
  region      = var.region
  zone        = var.zone
  environment = var.environment
}

module "gke" {
  source                 = "./modules/gke"
  project_id             = var.project_id
  depends_on             = [module.vpc]
  region                 = var.region
  zone                   = var.zone
  gke_initial_node_count = var.gke_initial_node_count
  gke_version            = var.gke_version
  gke_max_pods_per_node  = var.gke_max_pods_per_node
  gke_node_selector      = var.gke_node_selector
  environment            = var.environment
}

module "nodes" {
  source                 = "./modules/nodes"
  project_id             = var.project_id
  depends_on             = [module.gke]
  region                 = var.region
  zone                   = var.zone
  gke_initial_node_count = var.gke_initial_node_count
  gke_version            = var.gke_version
  gke_max_pods_per_node  = var.gke_max_pods_per_node
  gke_node_selector      = var.gke_node_selector
  environment            = var.environment
}