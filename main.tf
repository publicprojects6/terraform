terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.52.0"
    }
  }
  backend "gcs" {
    // alterar nome do bucket
    bucket = "_TERRAFORM_BUCKET_"
    prefix = "terraform/state/_CI_ENVIRONMENT_SLUG_"
  }
  required_version = ">= 0.14"
}

provider "google" {
  //  credentials = file("<NAME>.json")
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

provider "google-beta" {
  //  credentials = file("<NAME>.json")
  project = var.project_id
  region  = var.region
  zone    = var.zone
}
