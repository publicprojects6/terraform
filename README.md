### Projeto gke-multiple-nodes
Embora existam muitos outros, este projeto foi criado para implementar um cluster GKE ZONAL com múltiplos node groups e configurações personalizadas por node group em uma VPC personalizada de forma modularizada.

Este projeto está utilizando técnicas ensinadas no treinamento Descomplicado Terraform da LinuxTips ministrado pelo @gomex e sendo transcristo de AWS para GCP.

### O MAIS IMPORTANTE
Fique a vontade em contribuir com este projeto.

### Como trabalhar neste projeto com seu GCP
- Instale o Docker Desktop no seu computador, instruções no link - https://docs.docker.com/desktop/windows/install/;
- Criar uma conta no Google Cloud Platform;
- Criar uma organização;
- Criar um projeto dentro da organização criada no passo anterior;
- Crie um bucket no Cloud Storage para armazenar seu state;
- Crie um diretório para o state dentro do bucket;
- Altere o nome do seu bucket e o diretório raiz no arquivo main.tf;
- Criar uma conta de serviço com role Editor do projeto;
- Criar uma chave de acesso e salve no formato .json na raiz do projeto;

Sua chave .json é confidencial, portanto não altere o arquivo .gitignore para que a mesma não seja versionada no projeto no Gitlab.

### Executando container do Terraform no Docker Desktop no Windows

# POWERSHELL (GCP)
```
docker run --name terraform --rm -it \
  -v ${pwd}:/app \
  -w /app \
  -e GOOGLE_APPLICATION_CREDENTIALS=/app/<key>.json \
  --entrypoint sh hashicorp/terraform:light
```

# GIT BASH (GCP)
```
MSYS_NO_PATHCONV=1 docker run --name terraform --rm -it \
  -v $(pwd):/app \
  -w /app \
  -e GOOGLE_APPLICATION_CREDENTIALS=/app/<key>.json \
  --entrypoint sh  hashicorp/terraform:light
```

Substitua <key>.json pelo nome da sua chave.

Observe que o parametro --rm torna o container temporário, portanto sempre que executar um novo container execute o comando abaixo:
```
terraform init --upgrade
```
O arquivo .gitignore está configurado para não subir para este repositório o plan de nome "plano", portanto ao criar um novo plano execute o comando abaixo:
```
terraform plan --out plano
```

Para aplicar o seu plano execute o comando abaixo e forneça as informações comfome as instruções: 
```
terraform apply "plano"
```

Para remover o que foi criado pelo plano execute o comando abaixo e e forneça as informações conforme as instruções, depois digite 'yes' para confirmar;
```
terraform destroy
```

#### Pessoas que contribuem com este projeto

Dennye Garcia - https://www.linkedin.com/in/dennye-garcia/